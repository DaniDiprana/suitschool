package com.danidiprana.suitschool.API;

import com.danidiprana.suitschool.Model.Guest;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by ASUS on 31/03/2017.
 */

public interface GuestAPI {
    @GET("/people")
    public void getPeople(Callback<List<Guest>> response);
}
