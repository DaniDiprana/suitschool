package com.danidiprana.suitschool.Utils;

/**
 * Created by ASUS on 31/03/2017.
 */

public class Constants {

    public static final String BASE_URL = "http://dry-sierra-6832.herokuapp.com/api";
    public static final String USERNAME = "username";
    public static final String CHOOSE_EVENT = "choose_event";
    public static final String CHOOSE_GUEST = "choose_guest";
}
