package com.danidiprana.suitschool.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.danidiprana.suitschool.Activity.EventActivity;
import com.danidiprana.suitschool.R;
import com.danidiprana.suitschool.Model.Event;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by ASUS on 30/03/2017.
 */

public class EventAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater = null;
    public Resources res;
    Event tempValues = null;

    public EventAdapter(Activity a, ArrayList d, Resources resLocal){
        activity = a;
        data = d;
        res = resLocal;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return (data.size() < 0) ? 1 : data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView nama;
        public TextView tanggal;
        public ImageView image;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null){
            vi = inflater.inflate(R.layout.list_event, null);
            holder = new ViewHolder();
            holder.nama = (TextView) vi.findViewById(R.id.tv_list_name);
            holder.tanggal = (TextView) vi.findViewById(R.id.tv_list_date);
            holder.image = (ImageView) vi.findViewById(R.id.iv_list);
            vi.setTag(holder);
        }else{
            holder = (ViewHolder) vi.getTag();
        }

        if (data.size() <= 0) {
            holder.nama.setText("No Data");
        }else {
            tempValues = null;
            tempValues = (Event) data.get(position);

            holder.nama.setText(tempValues.getNama());

            String tgl = String.valueOf(tempValues.getTanggal().get(Calendar.DATE));

            DateFormatSymbols dfs = new DateFormatSymbols();
            String[] months = dfs.getMonths();
            String bulan = months[tempValues.getTanggal().get(Calendar.MONTH)].substring(0, 3);

            String tahun = String.valueOf(tempValues.getTanggal().get(Calendar.YEAR));

            holder.tanggal.setText(bulan + " " + tgl + " " + tahun);
            holder.image.setImageResource(
                    res.getIdentifier(
                            "com.danidiprana.suitschool:drawable/" + tempValues.getGambar()
                            , null, null));

            vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }


    @Override
    public void onClick(View v) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }

    private class OnItemClickListener  implements View.OnClickListener{
        private int mPosition;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
            EventActivity sct = (EventActivity) activity;
            sct.onItemClick(mPosition);
        }
    }
}
