package com.danidiprana.suitschool.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.danidiprana.suitschool.Activity.GuestActivity;
import com.danidiprana.suitschool.Model.Guest;
import com.danidiprana.suitschool.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 31/03/2017.
 */

public class GuestAdapter extends BaseAdapter implements View.OnClickListener {

    private Context activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    Guest tempValues=null;

    public GuestAdapter(Context a, ArrayList d,Resources resLocal) {
        activity = a;
        data=d;
        res = resLocal;

        inflater = ( LayoutInflater )activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView list_guest;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){
            vi = inflater.inflate(R.layout.list_guest, null);

            holder = new ViewHolder();
            holder.list_guest= (TextView) vi.findViewById(R.id.grid_item_icon);

            vi.setTag( holder );
        }
        else{
            holder=(ViewHolder)vi.getTag();
        }

        if(data.size()<=0)
        {
            holder.list_guest.setText("No Data");
        }
        else
        {
            tempValues = null;
            tempValues = (Guest) data.get(position);

            holder.list_guest.setText(tempValues.getName());

            vi.setOnClickListener(new OnItemClickListener(position));
        }
        return vi;

    }

    @Override
    public void onClick(View v) {
        Log.v("Guest Adapter", "=====Row button clicked=====");
    }

    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
            GuestActivity sct = (GuestActivity)activity;
            sct.onItemClick(mPosition);
        }
    }
}
