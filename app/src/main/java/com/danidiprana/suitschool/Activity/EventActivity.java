package com.danidiprana.suitschool.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.danidiprana.suitschool.Adapter.EventAdapter;
import com.danidiprana.suitschool.Fragment.FragmentMap;
import com.danidiprana.suitschool.Helper.Helper;
import com.danidiprana.suitschool.R;
import com.danidiprana.suitschool.Model.Event;
import com.danidiprana.suitschool.Utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;

public class EventActivity extends AppCompatActivity {

    private ListView list_view_event;
    private EventAdapter adapter;
    public EventActivity CustomListView = null;
    public ArrayList<Event> CustomListViewValuesArr = new ArrayList<Event>();
    private ImageButton btn_back;
    private ImageButton btn_new;
    private Fragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        CustomListView = this;

        setListData();

        //
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        View view = getSupportActionBar().getCustomView();

        btn_back = (ImageButton)view.findViewById(R.id.action_bar_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EventActivity.this, MenuAcitivity.class);
                startActivity(i);
            }
        });

        btn_new= (ImageButton)view.findViewById(R.id.action_bar_forward);

        btn_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EventActivity.this, "Map Clidked", Toast.LENGTH_LONG).show();
                FragmentMap mapFragment = new FragmentMap();
                getSupportFragmentManager().beginTransaction().replace(R.id.layout_event, mapFragment).commit();
            }
        });
        //


        Resources res = getResources();
        list_view_event = (ListView) findViewById(R.id.list_view_event);

        adapter = new EventAdapter(CustomListView, CustomListViewValuesArr, res);
        list_view_event.setAdapter(adapter);
    }

    public void setListData()
    {
        Calendar event_1_calendar = Calendar.getInstance();
        event_1_calendar.set(Calendar.YEAR, 2014);
        event_1_calendar.set(Calendar.MONTH, 11);
        event_1_calendar.set(Calendar.DATE, 9);
        CustomListViewValuesArr.add(new Event("Let Success Make The Noise", "dummy_1", event_1_calendar));

        Calendar event_2_calendar = Calendar.getInstance();
        event_2_calendar.set(Calendar.YEAR, 2014);
        event_2_calendar.set(Calendar.MONTH, 10);
        event_2_calendar.set(Calendar.DATE, 21);
        CustomListViewValuesArr.add(new Event("Semangat Tahun Baru", "dummy_2", event_2_calendar));

        Calendar event_3_calendar = Calendar.getInstance();
        event_3_calendar.set(Calendar.YEAR, 2014);
        event_3_calendar.set(Calendar.MONTH, 06);
        event_3_calendar.set(Calendar.DATE, 14);
        CustomListViewValuesArr.add( new Event("Work Hard In Smile", "dummy_3", event_3_calendar));

        Calendar event_4_calendar = Calendar.getInstance();
        event_4_calendar.set(Calendar.YEAR, 1998);
        event_4_calendar.set(Calendar.MONTH, 0);
        event_4_calendar.set(Calendar.DATE, 10);
        CustomListViewValuesArr.add(new Event("Another Message", "dummy_4", event_4_calendar));

        Calendar event_5_calendar = Calendar.getInstance();
        event_5_calendar.set(Calendar.YEAR, 1990);
        event_5_calendar.set(Calendar.MONTH, 7);
        event_5_calendar.set(Calendar.DATE, 9);
        CustomListViewValuesArr.add(new Event("Marlon Sitepu", "dummy_5", event_5_calendar));
    }

    public void onItemClick(int mPosition)
    {
        Event tempValues = (Event) CustomListViewValuesArr.get(mPosition);
        Helper.setCookie(CustomListView, Constants.CHOOSE_EVENT, tempValues.getNama());
        Intent intent = new Intent(CustomListView, MenuAcitivity.class);
        startActivity(intent);
    }

}
