package com.danidiprana.suitschool.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.danidiprana.suitschool.Helper.Helper;
import com.danidiprana.suitschool.R;
import com.danidiprana.suitschool.Utils.Constants;

public class MenuAcitivity extends Activity {

    private TextView tv_name;
    private Button btn_event;
    private Button btn_guest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_menu_acitivity);

        tv_name = (TextView) findViewById(R.id.tv_name);
        btn_event = (Button) findViewById(R.id.btn_pilih_event);
        btn_guest = (Button) findViewById(R.id.btn_pilih_guest);

        //Set nama di menu activity
        tv_name.setText("Nama           : " + Helper.getCookie(this, Constants.USERNAME));

        if (Helper.getCookie(this, Constants.CHOOSE_EVENT) != null){
            btn_event.setText(String.valueOf(Helper.getCookie(this, Constants.CHOOSE_EVENT)));
        }

        if (Helper.getCookie(this, Constants.CHOOSE_GUEST) != null){
            btn_guest.setText(String.valueOf(Helper.getCookie(this, Constants.CHOOSE_GUEST)));
        }

        //Button action
        btn_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuAcitivity.this, EventActivity.class);
                startActivity(i);
            }
        });

        btn_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuAcitivity.this, GuestActivity.class);
                startActivity(i);
            }
        });
    }
}
