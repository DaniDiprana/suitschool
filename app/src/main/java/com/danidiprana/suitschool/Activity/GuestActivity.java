package com.danidiprana.suitschool.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.danidiprana.suitschool.API.GuestAPI;
import com.danidiprana.suitschool.Adapter.GuestAdapter;
import com.danidiprana.suitschool.Helper.Helper;
import com.danidiprana.suitschool.Model.Guest;
import com.danidiprana.suitschool.R;
import com.danidiprana.suitschool.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GuestActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    GridView list_grid_guest;
    GuestAdapter adapter;
    public GuestActivity CustomListView = null;
    public List<Guest> guests;
    public ArrayList<Guest> CustomListViewValuesArr = new ArrayList<Guest>();
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        CustomListView = this;

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                    getPeople();
                }
            }
        );


        list_grid_guest = (GridView) findViewById(R.id.gridview);


    }

    public void getPeople() {
        swipeRefreshLayout.setRefreshing(true);
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching Data", "Please wait...", false, false);

        //Creating a rest adapter
        RestAdapter adpt = new RestAdapter.Builder()
                .setEndpoint(Constants.BASE_URL)
                .build();

        GuestAPI api = adpt.create(GuestAPI.class);
        api.getPeople(new Callback<List<Guest>>() {
            @Override
            public void success(List<Guest> list, Response response) {
                loading.dismiss();

                guests = list;

                restorePeople();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Retrofit_error", error.toString());
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    public void restorePeople(){
        for(int i=0; i<guests.size(); i++){
            Guest g = new Guest(guests.get(i).getName(), guests.get(i).getName());
            CustomListViewValuesArr.add(g);
        }

        Resources res = getResources();
        adapter = new GuestAdapter(CustomListView, CustomListViewValuesArr, res);
        list_grid_guest.setAdapter(adapter);
    }

    public void onItemClick(int mPosition)
    {
        Guest guest = guests.get(mPosition);
        Helper.setCookie(CustomListView, Constants.CHOOSE_GUEST, guest.getName());
        String date = formateDateFromstring("yyyy-MM-dd", "dd", guest.getBirthdate());
        String month = formateDateFromstring("yyyy-MM-dd", "MM", guest.getBirthdate());

        Intent intent = new Intent(CustomListView, MenuAcitivity.class);
        startActivity(intent);

        int d = Integer.parseInt(date);

        if ((d % 2 == 0) && (d % 3 == 0)) {
            Helper.showToast(GuestActivity.this, "iOS", Toast.LENGTH_LONG);
        } else if (d % 2 == 0) {
            Helper.showToast(GuestActivity.this, "Blackberry", Toast.LENGTH_LONG);
        } else if (d % 3 == 0) {
            Helper.showToast(GuestActivity.this, "Android", Toast.LENGTH_LONG);
        }else {
            phone_feature();
        }

        int m = Integer.parseInt(month);

        if (isPrime(m))
            Helper.showToast(GuestActivity.this, "Month is Prime", Toast.LENGTH_LONG);
        else
            Helper.showToast(GuestActivity.this, "Month is not Prime", Toast.LENGTH_LONG);

    }

    public boolean isPrime(int month){
        for (int i = 2 ; i < month ; i++)
            if (month % i == 0) return false;
        return true;
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;

    }

    public void phone_feature(){
        Intent i = new Intent(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:087822044096"));
        if (ActivityCompat.checkSelfPermission(GuestActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(i);
    }


    @Override
    public void onRefresh() {
        getPeople();
    }
}
