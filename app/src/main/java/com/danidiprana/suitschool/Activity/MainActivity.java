package com.danidiprana.suitschool.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.danidiprana.suitschool.Helper.Helper;
import com.danidiprana.suitschool.R;
import com.danidiprana.suitschool.Utils.Constants;

public class MainActivity extends Activity implements View.OnClickListener {

    private Activity activity;
    private EditText et_name;
    private Button btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        activity=this;
        init();

        et_name = (EditText) findViewById(R.id.et_name);
        btn_next = (Button) findViewById(R.id.btn_next);

        btn_next.setOnClickListener(this);
    }

    public void init(){
        Helper.setCookie(activity, Constants.USERNAME, null);
        Helper.setCookie(activity, Constants.CHOOSE_EVENT, null);
        Helper.setCookie(activity, Constants.CHOOSE_GUEST, null);
    }

    @Override
    public void onClick(View v) {
        Helper.setCookie(activity, Constants.USERNAME, String.valueOf(et_name.getText()));

        if (!String.valueOf(et_name.getText()).isEmpty()) {
            if (isPalindrome(String.valueOf(et_name.getText())))
                Helper.showToast(MainActivity.this, "Name is Palindrome", Toast.LENGTH_LONG);
            else
                Helper.showToast(MainActivity.this, "Name is not Palindrome", Toast.LENGTH_LONG);
        }

        Intent intent = new Intent(this, MenuAcitivity.class);
        startActivity(intent);
    }

    public boolean isPalindrome(String name){
        String nama = name.replaceAll("\\s+","");
        char [] arrName = nama.toLowerCase().toCharArray();
        int i = 0,j = nama.length()-1;
        while (j > i){
            if (arrName[i] != arrName[j]) return false;
            i++; j--;
        }
        return true;
    }
}
