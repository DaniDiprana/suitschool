package com.danidiprana.suitschool.Model;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by ASUS on 30/03/2017.
 */

public class Event {

    private String nama;
    private String gambar;
    private Calendar tanggal;

    public Event(){

    }

    public Event(String nama, String gambar, Calendar tanggal) {
        this.nama = nama;
        this.gambar = gambar;
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public Calendar getTanggal() {
        return tanggal;
    }

    public void setTanggal(Calendar tanggal) {
        this.tanggal = tanggal;
    }
}
