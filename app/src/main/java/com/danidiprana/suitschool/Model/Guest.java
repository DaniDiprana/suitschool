package com.danidiprana.suitschool.Model;

import java.util.Calendar;

/**
 * Created by ASUS on 31/03/2017.
 */

public class Guest {

    private String name;
    private String birthdate;

    public Guest(){

    }
    public Guest(String name, String birthdate) {
        this.name = name;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
