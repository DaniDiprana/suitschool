package com.danidiprana.suitschool.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * Created by ASUS on 31/03/2017.
 */

public class Helper {

    public static final String PREFS_NAME = "PrefsFile";

    public static void setCookie(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static Object getCookie(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }

    public static void showToast(Context context, String message, int length) {
        Toast.makeText(context, message, length).show();
    }
}
